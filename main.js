// Add JS here
function cambiarContenido(sectionId) {
    var trayectoria1 = document.getElementById(`trayectoria1-${sectionId}`);
    var trayectoria2 = document.getElementById(`trayectoria2-${sectionId}`);
    var botonCambiar = document.getElementById(`botonCambiar-${sectionId}`);
    var scrollPosition = window.scrollY; // Guardar la posición de desplazamiento actual

    // Ocultar el contenido actual y mostrar el nuevo contenido
    if (trayectoria1.classList.contains('hidden')) {
        trayectoria1.classList.remove('hidden');
        trayectoria2.classList.add('hidden');
    } else {
        trayectoria1.classList.add('hidden');
        trayectoria2.classList.remove('hidden');
    }

    // Cambiar el texto del botón dinámicamente
    if (botonCambiar.innerText === "Ver resumen") {
        botonCambiar.innerText = "Volver";
    } else {
        botonCambiar.innerText = "Ver resumen";
    }

    // Restaurar la posición de desplazamiento después del cambio
    window.scrollTo(0, scrollPosition);
}



